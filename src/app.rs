use crate::{suite_manager::{FileStatus, FileInfo}, TestSuite};
use egui::Ui;
use std::{env, fs, process::exit};
use tracing::{error, info};

pub struct SuiteApp {
    suite: TestSuite,
}

impl Default for SuiteApp {
    fn default() -> Self {
        let args: Vec<_> = env::args().collect();
        if args.len() == 1 {
            error!("Project path was not provided");
            eprintln!("Usage: <project-path>");
            exit(1);
        }

        let mut s = TestSuite::new(args.last().unwrap().to_string());
        s.build();

        Self { suite: s }
    }
}

impl SuiteApp {
    pub fn new(_cc: &eframe::CreationContext<'_>) -> Self {
        Default::default()
    }

    pub fn get_file_contents(&self, file: &String) -> String {
        let c = fs::read_to_string(file).unwrap();
        return c;
    }
}

impl eframe::App for SuiteApp {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        // Put your widgets into a `SidePanel`, `TopBottomPanel`, `CentralPanel`, `Window` or `Area`.

        egui::TopBottomPanel::top("top_panel").show(ctx, |ui| {
            egui::menu::bar(ui, |ui| {
                ui.menu_button("File", |ui| {
                    if ui.button("Quit").clicked() {
                        ctx.send_viewport_cmd(egui::ViewportCommand::Close);
                    }
                });
                ui.add_space(16.0);
            });
        });

        egui::SidePanel::left("left_panel")
            .resizable(true)
            .default_width(500.0)
            .width_range(80.0..=500.00)
            .show(ctx, |ui| {
                ui.vertical(|ui| {
                    ui.heading("Tests");

                    for f in &self.suite.test_files {
                        ui.horizontal(|ui| {
                            ui.label(match f.status {
                                FileStatus::NEW => "O",
                                FileStatus::FAIL => "F",
                                _ => "X"
                            });
                            if ui.label(f.path.to_string()).clicked() {
                                info!("Label clicked");
                                self.suite.current_file = Some(f.clone());
                            }
                        })  ;
                    }
                })
            });

        egui::CentralPanel::default().show(ctx, |ui| match &self.suite.current_file {
            Some(f) => {
                let ps = f.path.to_string();

                if ui.button("Test").clicked() {
                    self.suite.run_test_file(&ps);
                }

                let mut c = self.get_file_contents(&ps);
                ui.text_edit_multiline(&mut c);
            }
            None => (),
        });
    }
}
