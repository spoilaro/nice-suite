#![warn(clippy::all, rust_2018_idioms)]

mod app;
mod suite_manager;

pub use app::SuiteApp;
pub use suite_manager::TestSuite;
