use std::fs;
use std::ops::Add;
use std::path::PathBuf;
use std::process::Command;
use tracing::{info, debug, error};

/// Enum to describe if the tests in the file have
/// already been run.
#[derive(Debug, Clone)]
pub enum FileStatus {
    SUCCESS,
    FAIL,
    NEW,
}

/// Struct to contain information about a test file.
#[derive(Debug, Clone)]
pub struct FileInfo {
    pub path: String,
    pub tests: Vec<String>,
    pub status: FileStatus,
}

/// Main strcuture for the suite. This is what is imported and used
/// by other libraries.
pub struct TestSuite {
    test_path: String,
    pub test_files: Vec<FileInfo>,
    pub current_file: Option<FileInfo>
}

impl TestSuite {
    pub fn new(project_path: String) -> Self {

        info!("Initializing test suite, path={}/", project_path);
        let test_path = project_path.add("/tests");

        TestSuite {
            test_path,
            test_files: Vec::new(),
            current_file: None
        }
    }

    pub fn build(&mut self) {

        info!("Building test files");
        match fs::read_dir(&mut self.test_path) {
            Ok(entries) => {
                for entry in entries {
                    match entry {
                        Ok(entry) => {
                            let is_file = match entry.metadata() {
                                Ok(data) => data.is_file(),
                                Err(_) => false,
                            };

                            if is_file == true {
                                self.test_files.push(FileInfo {
                                    path: self.path_to_string(entry.path()),
                                    tests: self.parse_test_names_from_file(entry.path()),
                                    status: FileStatus::NEW,
                                })
                            }
                        }
                        Err(e) => error!("Unexpected error, error: {}", e),
                    }
                }
            }
            Err(e) => {
                error!("Could not read files from directory, error: {}", e);
            }
        }
    }

    pub fn run_test_file(&mut self, test_file: &String) {
        info!("Running test file, file: {}", test_file);
        let file_name = test_file.split("/").last().unwrap().replace(".py", "");

        let report_arg = format!(
            "--junitxml={}/reports/{file_name}_report.xml",
            self.test_path
        );

        Command::new("test_project/env/bin/python3")
            .args(["-m", "pytest", &report_arg, &test_file])
            .output()
            .expect("Could not execute pytest");

        for f in &mut self.test_files {
            if f.path == test_file.clone() {
                info!("Assigning SUCCESS to '{}'", f.path);
                f.status = FileStatus::SUCCESS;
            }
        }

    }

    /// Parses names of the test functions from the test file. Used to get the names when Pytest
    /// hasn't been run yet.
    fn parse_test_names_from_file(&self, file_path: PathBuf) -> Vec<String> {
        let contents = fs::read_to_string(file_path).expect("Could not read test file");

        let tests = contents
            .split('\n')
            .filter(|l| l.starts_with("def test_"))
            .map(|l| String::from(l))
            .collect();

        return tests;
    }

    fn path_to_string(&self, path: PathBuf) -> String {
        <PathBuf as Clone>::clone(&path)
                    .into_os_string()
                    .into_string()
                    .unwrap()
    }
}
